import React from 'react'

function Home() {
    return (
        
        <div id="home" className="h-screen py-5 px-3 md:p-20 flex">

            <div className="w-full h-full border border-slate-200 flex shadow-sm rounded-xl items-center flex-col-reverse md:flex-row hover:shadow-2xl  duration-500 hover:border-gray">
                
                <span className="h-full w-1/2 flex items-center justify-center">
                    <div className="h-fit">
                        <h1 className="font-extrabold text-xl md:text-4xl">Hi</h1>
                        <h1 className="font-extrabold text-xl md:text-4xl mb-6">I am <span className="text-blue-600">Ruhullah Reza</span></h1>
                        <a  href="#about" className="px-4 py-2 bg-blue-600 rounded-lg text-white shadow-lg shadow-blue-500/80 hover:bg-blue-800 hover:shadow-none duration-300 cursor-pointer">About Me</a>
                    </div>
                </span>               

                <span className="h-3/4 w-1/2 md:h-fit md:w-1/2 py-5 md:py-0 flex items-center justify-center">
                    
                    <img src="/profile_plain.png" alt="" className="h-6/6 w-6/6 md:h-3/6 md:w-3/6 bg-blue-600 rounded-full shadow-xl shadow-blue-600/70 duration-500 hover:shadow-none "/>

                </span>

            </div>
        
        </div>



    )
}

export default Home

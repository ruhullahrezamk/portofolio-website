import React from 'react'
import '../index.css'

function Navbar() {

    return (

        <div className="flex justify-between shadow-lg px-28 rounded-lg fixed inset-x-0 bg-white invisible md:visible">

            <a href="#home" className="p-3 text-2xl cursor-pointer hover:text-blue-600 duration-500">Ozza</a>


            <div className="flex justify-between">

                <a href="#home" className="px-3 py-4 border-l-2 border-gray-200 flex align-center hover:bg-blue-500 hover:text-white duration-500 cursor-pointer">Home</a>
                <a href="#about" className="px-3 py-4 border-l-2 border-gray-200 flex align-center hover:bg-blue-500 hover:text-white duration-500 cursor-pointer">About Me</a>
                <a href="#experience" className="px-3 py-4 border-x-2 border-gray-200 flex align-center hover:bg-blue-500 hover:text-white duration-500 cursor-pointer">Experience</a>
                <a href="#project" className="px-3 py-4 border-x-2 border-gray-200 flex align-center hover:bg-blue-500 hover:text-white duration-500 cursor-pointer">Project</a>
            
            </div>
            
        </div>
    )
}

export default Navbar

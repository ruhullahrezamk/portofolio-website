import React from 'react'

const loading = () => {
    return (
        <div class="flex items-center justify-center space-x-2 animate-bounce">
            <div class="w-4 h-4 bg-red-400 rounded-full"></div>
            <div class="w-4 h-4 bg-green-400 rounded-full"></div>
            <div class="w-4 h-4 bg-blue-400 rounded-full"></div>
        </div>
    )
}

export default loading

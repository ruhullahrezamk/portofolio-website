import React from 'react'

const About = () => {
    return (
        <div id="about" className="h-screen py-5 px-3 md:py-24 md:px-20 flex">

            <div className="w-full h-full p-12 border border-slate-200 flex shadow-sm rounded-xl items-center flex-col md:flex-row hover:shadow-2xl duration-500 hover:border">
            
                <div className="h-full w-full p-3">

                    <h1 className="font-bold text-sm md:text-lg mb-1"><span className="text-blue-600">Professional Summary</span></h1> 
                    <p className="font-light text-md text-justify mb-3">A mathematics fresh graduate with 1-year of experience as a backend engineer, I bring a passion for software development, software security, and machine learning. With a proven track record in developing SaaS products, I am skilled in API and service development and quite knowledgeable in software security field. I have a strong problem-solving mindset and a love for crunching numbers. I am motivated to continue learning, taking on new challenges, and using my programming skills to solve real-world business problems. I seek opportunities to work with a team of talented professionals who share my passion for technology and problem-solving.</p>

                    <h1 className="font-bold text-sm md:text-lg mb-1"><span className="text-blue-600">Education</span></h1> 
                    <p className="font-light text-md text-justify">Bachelor of Mathematics, Universitas Pendidikan Indonesia (Sep 2019 – April 2023)</p>
                    <p className="font-light text-md text-justify mb-3">GPA 3.82</p>

                    <h1 className="font-bold text-sm md:text-lg mb-1"><span className="text-blue-600">Technical Skills</span></h1> 
                    <p className="font-light text-md text-justify mb-3">GO, NodeJs, Python, SQL, PostgreSQL, Git </p>

                    <h1 className="font-bold text-sm md:text-lg mb-1"><span className="text-blue-600">Soft Skills</span></h1> 
                    <p className="font-light text-md text-justify mb-3">Problem Solving, Critical Thinking, Adaptability, Teamwork, Communication </p>

                    <h1 className="font-bold text-sm md:text-lg mb-1"><span className="text-blue-600">Language</span></h1> 
                    <p className="font-light text-md text-justify"> Indonesia (Native), English (TOEFL Overall score 567 out of 677)</p>
      
                </div>   

            

            </div>

        </div>
    )
}

export default About

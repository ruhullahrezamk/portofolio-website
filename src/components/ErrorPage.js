import React from 'react'

const ErrorPage = () => {
    return (
        <div id="error" className="h-screen py-5 px-3 md:p-20 flex">

        <div className="w-full h-full rounded-xl items-center justify-center flex md:flex-row shadow-2xl  border-gray">
            
           <span className='md:w-2/5 md:h-2/5 md:py-10 font-mono text-center'>
               <p className='text-8xl font-bold mb-5'>404</p>
               <p className='mb-8'>That Page Doesn't Exist or is Unavailable</p>
               <a  href="/" className="px-4 py-2 bg-white rounded-2xl text-black border border-blue-800 hover:bg-blue-800 hover:text-white duration-500 cursor-pointer">Home</a>
           </span>

        </div>
    
    </div>
    )
}

export default ErrorPage

import React from 'react'

const Project = () => {

    return (
        <div id="project" className="h-screen py-5 px-3 md:py-24 md:px-20 flex">


            <div className="w-full h-full p-12 border border-slate-200 flex shadow-sm rounded-xl items-center flex-col md:flex-row hover:shadow-2xl duration-500 hover:border">
                        
                <span className="h-full w-full md:w-1/2 p-3 flex">
                    <div className="h-fit">
                        <h1 className="font-bold text-sm md:text-lg mb-3"><span className="text-blue-600">Central Admin</span></h1> 
                        <p className="font-light text-md text-justify mb-3">Central Admin is an application for managing events, participant attendance, and portfolios for PT Central Artificial Intelligence. In managing events and portfolios, when a user adds data, a notification email will be sent to the admin for review before publishing. The feature for managing participant attendance data is integrated with the system for creating certificates and sending them automatically via email. In this project, I have responsibility for designing the database, developing RESTful API, writing documentation, and fixing bugs.</p>

                    </div>
                </span>

                <span className="h-full w-full md:w-1/2 p-3 flex">
                    <div className="h-fit">
                        <h1 className="font-bold text-sm md:text-lg mb-3"><span className="text-blue-600">Second Hand </span></h1> 
                        <p className="font-light text-md text-justify mb-3">Second Hand is an e-commerce website for buying and selling used goods, where users can sell their used goods and also make an offer to buy used goods from other users. If the seller accepts the offer from the buyer, then they can continue the transaction using WhatsApp. This website was created as the final project of the Backend Javascript program at Binar Academy. On this project, I have responsibility for designing the database, developing RESTful API, fixing bugs, CI/CD using Gitlab, and deploying to Heroku.</p>
                        <p className="font-light text-md text-justify mb-3">Source Code</p>
                        <ul className='list-disc ml-5'>
                            <li className='mb-2'>GO-Lang: <a className='hover:text-sky-600 duration-200' href='https://github.com/RuhullahReza/SecondHand'>https://github.com/RuhullahReza/SecondHand</a></li>
                            <li className='mb-2'>NodeJS : <a className='hover:text-sky-600 duration-200' href='https://gitlab.com/ruhullahrezamk/final-project-kelompok-5'>https://gitlab.com/ruhullahrezamk/final-project-kelompok-5</a></li>
                        </ul>
                    </div>
                </span>
                        
            </div>
        </div>
    )

}

export default Project

import React from 'react'

const Experience = () => {

    return (
        <div id="experience" className="h-screen py-5 px-3 md:py-24 md:px-20 flex">

            <div className="w-full h-full p-12 border border-slate-200 flex shadow-sm rounded-xl items-center flex-col md:flex-row hover:shadow-2xl duration-500 hover:border">
            
                <span className="h-full w-full  md:w-1/2 p-3 flex ">
                    <div className="h-fit">
                        <h1 className="font-bold text-sm md:text-lg mb-3"><span className="text-blue-600">PT Central Artificial Intelligence</span></h1> 

                        <ul className="list-disc ml-5">
                            <li className='mb-2'>Designed and built databases for internal web applications.</li>
                            <li className='mb-2'>Building, Developing and maintaining API for internal web applications.</li>
                            <li className='mb-2'>Collaborate with the Data Science team on integrating machine learning models with API that will generate a new product.</li>
                            <li className='mb-2'>Enable new payment method on Central AI by building API for QRIS payment.</li>
                            <li className='mb-2'>Implemented clean code, which made code in contributed projects become highly readable, easy to maintain, and modular.</li>
                        </ul>
                    </div>
                </span>

                <span className="h-full w-full md:w-1/2 p-3 flex">
                    <div className="h-fit">
                        <h1 className="font-bold text-sm md:text-lg mb-3"><span className="text-blue-600">Binar Academy</span></h1> 

                        <ul className="list-disc ml-5">
                            <li className='mb-2'>Designed and built a database for applications for e-commerce (including notifications for users)</li>
                            <li className='mb-2'>Building, developing and testing e-commerce API.</li>
                            <li className='mb-2'>CI CD using Gitlab and deploying API on Heroku.</li>
                            <li className='mb-2'>Wrote API contracts and documentation to satisfy the Agile method within development team.</li>
                            <li className='mb-2'>Wrote unit tests and integration tests</li>
                        </ul>
                    </div>
                </span>
                
            </div>
        </div>
    )

}

export default Experience

import Navbar from './components/Navbar';
import Home from './components/Home';
import About from './components/About';
import Project from './components/Project';
import Experience from './components/Experience';

function App() {

  document.body.classList.add('scrollbar-hide','font-mono');
  
  return (

    <div className='pb-10 sm:pb-0'>

      <Navbar />

      <Home/>

      <About/>

      <Experience/>

      <Project/>
      
    </div>

  );
  
}

export default App;

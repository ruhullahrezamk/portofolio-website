module.exports = {
  content: [
    './src/App.js',
    './src/components/Navbar.js',
    './src/components/Home.js',
    './src/components/About.js',
    './src/components/Project.js',
    './src/components/Loading.js',
    './src/components/ErrorPage.js',
  ],
  theme: {
    extend: {},
  },
  plugins: [
    require('tailwind-scrollbar-hide')
  ],
}
